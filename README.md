# Configure Local Nagios configuration validation
The nagios-config migration from git.stanford.edu to code.stanford.edu removed the ability to validate the config before changes are pushed to the repository (pre-receive hook).  However, following the steps below will allow nagios config changes to be validated on the client side before the commit is allowed.  It employs a Nagios container which is used to run a nagios config validation on the local config repository to make sure all config information is syntactically correct.

## Support Matrix

| Supported Operating Systems | Additional information |
| ------ | ------ |
| [Linux](#local-config-validation-on-linux) | Tested and verified with Debian bullseye, should work fine for other flavors with possible slight package differences |
| [Microsoft](#local-config-validation-on-windows) | Windows 10, version 1903 or higher or Windows 11.  **Note: This will NOT work on vDesktop** |
| [MacOS](#local-config-validation-on-mac) | Tested and verified on macOS Monterey Version 12.4 |

## Currently available Nagios config repositories 
Main UIT Nagios config (onprem): https://code.stanford.edu/monitoring/nagios/nagios-config<br>
Kubernetes Nagios config (cloud): https://code.stanford.edu/monitoring/knagios/knagios-config

## Local config validation on Linux
1. Install required packages
    - `apt-get update`
    - `apt-get install git docker.io`
1. The ability to run docker will be required for this to work.  If elevated permissions are not available, add the current user to the docker group.
1. Clone a nagios-config repo via SSH
    - `git clone git@code.stanford.edu:monitoring/nagios/nagios-config.git`
1. Change to the githooks directory
    - `cd nagios-config/.git/hooks`
1. Download the pre-commit hook
    - `wget https://code.stanford.edu/monitoring/nagios-local-config-validation/-/raw/main/pre-commit`
    - `chmod 755 pre-commit`
1. Create a new branch, make changes.  Any commits now will first be validated via nagios before being allowed to successfully complete

## Local config validation on Windows
1. Install the Windows Subsystem for Linux (WSL) for Debian
    - Open up a PowerShell Admin Command Prompt
    - `wsl --install -d Debian`
    - Reboot your System
    - Create a default UNIX user account and password
1. Install required packages
    - `sudo apt-get update`
    - `sudo apt-get install git`
1. Install Docker Desktop and integrate with WSL2
    - Follow the instructions here: https://docs.docker.com/desktop/windows/wsl/#:~:text=Docker%20Desktop%20uses%20the%20dynamic,container%20to%20run%20much%20faster.
1. Configure access to code.stanford.edu
    - Create an SSH key (ed25519 preferred): https://code.stanford.edu/help/user/ssh.md#generate-an-ssh-key-pair
    - Paste the generate public key here: https://code.stanford.edu/-/profile/keys
    - Click the "Add Key" button
1. Clone a nagios-config repo via SSH
    - `git clone git@code.stanford.edu:monitoring/nagios/nagios-config.git`
1. Change to the githooks directory
    - `cd nagios-config/.git/hooks`
1. Download the pre-commit hook
    - `wget https://code.stanford.edu/monitoring/nagios-local-config-validation/-/raw/main/pre-commit`
    - `chmod 755 pre-commit`
1. Create a new branch, make changes.  Any commits now will first be validated via nagios before being allowed to successfully complete

## Local config validation on MacOS
1. Install git
    - `brew install git` or follow instructions here: https://git-scm.com/download/mac
1. Install docker desktop
    - https://docs.docker.com/desktop/install/mac-install/
1. Clone a nagios-config repo via SSH
    - `git clone git@code.stanford.edu:monitoring/nagios/nagios-config.git`
1. Change to the githooks directory
    - `cd nagios-config/.git/hooks`
1. Download the pre-commit hook
    - `curl https://code.stanford.edu/monitoring/nagios-local-config-validation/-/raw/main/pre-commit -o pre-commit`
    - `chmod 755 pre-commit`
1. Create a new branch, make changes.  Any commits now will first be validated via nagios before being allowed to successfully complete
